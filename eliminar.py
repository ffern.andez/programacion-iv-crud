import sqlite3
conn=sqlite3.connect("slang")
cursor=conn.cursor()
tablas="SELECT *, rowID FROM diccionario;"
cursor.execute(tablas)
diccionario=cursor.fetchall()
print("+{:-<20}+{:-<50}+{:-<10}+".format("", "", ""))
print("|{:^20}|{:^50}|{:^10}|".format("Palabra", "Significado", "ID"))
print("+{:-<20}+{:-<50}+{:-<10}+".format("", "", ""))
for palabras, significado, rowid in diccionario:
    print("|{:^20}|{:^50}|{:^10}|".format(palabras, significado, rowid))
print("+{:-<20}+{:-<50}+{:-<10}+".format("", "", ""))
id_palabra=input("\nEscribe el ID de la palabra que quieres eliminar: ")
if not id_palabra:
	print("No escribiste nada")
	exit()
tablas="DELETE FROM diccionario WHERE rowID = ?;"
cursor.execute(tablas, [id_palabra])
conn.commit()
conn.close()