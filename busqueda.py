import sqlite3
conn=sqlite3.connect("slang")
cursor=conn.cursor()
search=input("Escribe tu búsqueda: ")
if not search:
	print("Búsqueda inválida")
	exit()
tablas="SELECT * FROM diccionario WHERE palabras LIKE ?;"
cursor.execute(tablas, ["%{}%".format(search)])
diccionario=cursor.fetchall()
print("+{:-<20}+{:-<50}+".format("", ""))
print("|{:^20}|{:^50}|".format("Palabra", "Significado"))
print("+{:-<20}+{:-<50}+".format("", ""))
for palabras, significado in diccionario:
    print("|{:^20}|{:^50}|".format(palabras, significado))
print("+{:-<20}+{:-<50}+".format("", ""))
conn.close()